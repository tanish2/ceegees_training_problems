let box = document.querySelector(".fibonaci");
let fibNum = [0, 1];
let seriesLength = 15;

document.getElementById("btn").addEventListener("click", () => {
  box.innerHTML = "";
  let N = document.querySelector("#NCount").value;
  if (N === "") {
    alert("Please enter N");
  }
  else {
    let sum = 1;
    for (let i = 2; i < N; i++) //Filling array for the first N numbers
    {
      fibNum[i] = fibNum[i - 1] + fibNum[i - 2];
      sum += fibNum[i];
    }

    let startSum = 0;
    let j = -1;
    for (let i = N; i < seriesLength; i++) {
      fibNum[i] = sum - startSum;
      startSum += fibNum[++j];
      sum += fibNum[i];
    }

    addFiboNumbers(); //displaying numbers in the displayBox
  }
});

function addFiboNumbers() {
  for (let i = 0; i < fibNum.length; i++) {
    setTimeout(() => {
      box.innerText += ` ${fibNum[i]},`;
    }, i * 500);
  }
}